After the boring stuff we have survived and reached some thing that we can run


**Things That we should try to cover**
- [X] Hello world in JS
- [X] var in JS
- [ ] basics of Data type. This has many memory implications. try to touch those
- [x] if in JS
- [ ] Data types in js see link [mdn link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)
- [ ] string in js see link [mdn link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)
- [ ] diff between statement and expression
- [ ] operators in JS
- [ ] loop in JS
- [ ] functions in JS
- [ ] high level array overview. We will see it in detail in next sessions
- [ ] An very high level overview of AST. Refer to [ast explorer link, this contains the parser](https://astexplorer.net/)
- [ ] testing our code
- [ ] trying to handle broken code
- [ ] trying to fix a broken codes


Questions
1. Create a function that will print sum of 2 numbers.
2. Create a function that will take an array as in input and return sum
3. Give sum of functions that we give sum of nums which are greater than 100

This can be the home assignment if we do not have time for this
4. Final problem. given an array to a func you have to return back resultant array of same size.
For every number you have do certain operations. if number is odd add nearest prime number to it. if the number is even add 2nd nearest prime number.
A prime number is a number which is only divisible by 1 and the number itself. You have 25 mins to do so.


you have to write a function which takes 2 inputs 
1. input is a source point
2. input is an array of points (dest array)

point is an object with 2 properties x and y; the point here can be referred as point on a 2d plane
Your function should return the nearest point in the dest array
//for distance between a point try using formula √ (x2 – x1) + (y2 – y1) 2
example:
```
src = {x:0, y:0}
dest arr = [{x:100 ,y:0}, {x:200, y:10}]
result = {x:100 ,y:0}
```


write a program that takes starting address(every address is an integer) and inital size of array. 
every element in the array takes 4 bytes. you will also get array of operations `add` or `remove`
add is basically adding to the array and removing is basically removing from the array
you finally have to print total size in bytes and starting and ending addresses 

implement a stack using array. try to avoid just using push and pop

create mathematical calculator for +, - , * and / take complex inputs as string and create calculator which maintains mathematical precedence
your input will be a string  eg. '10+10' or '10+10*2' and you have to return the result of the calculation