package main

import "fmt"

func test(num int) {
	fmt.Printf("the number passed here is %d", num)
}

func main() {
	test(10)
	//test(true)
	//test("garbage")
}
