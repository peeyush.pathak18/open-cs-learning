// interpreted language functioning
function test(x) {
  console.log("the number passed here is ", x);
}

test(10);
test(true);
test("garbage");


//basic logging functions
console.log("Hello world, we have to do the ritual :) ");

var x = 0;
console.log(x);

if (x > 10) {
  console.log("x is greater");
}
else if (new Date("Wed Jul 16 2020 18:00:00 GMT+0200 (Central European Summer Time)") > new Date()){
  console.log("We still have some time");
}
else {
  console.log("x is smaller");
}

// handy operators
/**
 * + - * / %
 * >> <<
 * && ||
 * & |
 */

var y = 10;

while(y>0){
  console.log("value of y is ", y);
  y--;
}


for (
    var z = 0; // 1st part conventionally used for initialisation
    z<10; // 2nd part contains the condition
    z++ //3rd is conventionally used for doing operation that might affect the condition evaluation
) {
  console.log("value of x is ", x);
}
/*
Order of iteration of for loop
1
2 condition returns true
3
2 condition returns true
3
2 condition returns true
3
2 condition returns true
3
2 // if this time condition returns false then it exits the for loop block


 */



var a = 0;
do {
  console.log('value of a is', a);
}while(a > 0);



function fn1() {
  console.log("I am function 1");
}


var arr = [];
for (var i=0; i<1000; i++) {
  arr.push(parseInt(Math.random() * 1000));
}



