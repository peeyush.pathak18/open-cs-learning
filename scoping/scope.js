/**
Initial points that I want to cover
 . context(this)
 . global scope
 . function scope
 . block level scope(will see later wth let and const)
 . hoisting, what is hoisted and what is not
 . why hoisting
 . closure, lets see closure



 next sessions
 . assigning the context using call, apply and bind
 . prototype
 . prototype chain
 . prototype chain hell
 */

/**
 * Context examples
 */
console.log("the value of this is ", this);

{
  console.log("value if this inside block ", this);
}

/*
By default variables are taken from local scope otherwise it is searched on global context
 */
iAmLeaked = "this value should get attached to window scope";
console.log("on this scope ", this.iAmLeaked);
console.log("on window scope ", window.iAmLeaked);


function classFunction(){
  this.x = 10;
  this.y = 20;
}


var ob = new classFunction();
console.log("ob value is ", ob);
ob.x = 60;
console.log("value of ob ", ob);


var no_ob = classFunction();
console.log("non ob value is ", no_ob);

var old_ob = {
  x: 40,
  y: 50
};

console.log("the value of old ob is ", old_ob);
old_ob.x = 99;
console.log("the value of old ob is ", old_ob);



/**
 The value in before case will be undefined but it will not throw error
 the answer to this is hoisting
 so after parsing when js is interpreted it is interpreted like this:
 x get declared as first line of function
 by default x is assigned undefined, the reason for undefined is because this makes it clear it is still not perfectly defined
 x is assigned at the line where x is declared and assigned.
 */
function mainFunc() {
  function test(prefix) {
    console.log(prefix, x);
  }
  test("before");
  var x = 10;
  test("after");
}


/**
 calling this function will throw an error
 the error occurs because there is no x locally or globally
 */
function mainFuncWrong() {
  function test(prefix) {
    console.log(prefix, x);
  }
  test("only");
}

