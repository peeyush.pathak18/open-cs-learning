function NodeCreator(value) {
  this.value = value;
  this.next = null;

  this.addNext = function (next) {
      this.next = next;
  }
}

function DoubleNodeCreator(value) {
  this.value = value;
  this.next = null;
  this.prev = null;

  this.addNext = function (next) {
    this.next = next;
  };

  this.addPrev = function (prev) {
    this.prev = prev;
  };
}

var x = 10;

var y = {};

// {} 1234

//storing
node = {
  value: 0,
  next: null
};

var root  = new NodeCreator(1);
var currentNode = root;
for (var i=2; i<10; i++) {
  var newNode = new NodeCreator(i);
  currentNode.addNext(newNode);
  currentNode = newNode;
}

//iterating / reading
currentNode = root;
while (currentNode) {
  console.log(currentNode.value);
  currentNode = currentNode.next;
}

// {root 1} -> {2} -> {3} -> {4}
// remove {2} // remove arr[1]

// {root 1} -> {3} -> {4}

root.next //3
newNode //2

newNode.next = root.next;
root.next = newNode;

// {root 1} -> {2}-> {3} -> {4}



//iterating / reading
currentNode = root;
// requiredIndex = 2;
// currIndex = 0;
while (node) {
  // if (currIndex === requiredIndex) {
  //   console.log(currentNode.value);
  // }
  // currIndex ++;

  console.log(currentNode.value);
  currentNode = currentNode.next;
}